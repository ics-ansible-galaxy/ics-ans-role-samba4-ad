# ics-ans-role-samba4-ad

Ansible role to install Samba4 as an active directory domain controller. 

## Role Variables

```yaml
...
smb_repository: default
smb_role: member
smb_workgroup: mshome
smb_realm: mshome.net
smb_dns_servers: 192.168.1.1
smb_dns_forwarder: 8.8.8.8
smb_username: vagrant
smb_password: Vagrant54321
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-samba4-ad
```

## License

BSD 2-clause
